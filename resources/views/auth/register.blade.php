@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
     <!-- preloader begin -->
    <div class="in-loader">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <!-- preloader end -->
    <main>
        <!-- section content begin -->
        <div class="uk-section uk-padding-remove-vertical">
            <div class="uk-container uk-container-expand">
                <div class="uk-grid" data-uk-height-viewport="expand: true">
                    <div class="uk-width-3-5@m uk-background-cover uk-background-center-right uk-visible@m uk-box-shadow-xlarge" style="background-image: url(/img/in-signin-image.jpg);">
                    </div>
                    <div class="uk-width-expand@m uk-flex uk-flex-middle">
                        <div class="uk-grid uk-flex-center">
                            <div class="uk-width-3-5@m">
                                <div class="in-padding-horizontal@s">
                                    <!-- module logo begin -->
                                    <a class="uk-logo" href="{{url('/')}}">
                                        <img class="in-offset-top-10" src="/img/logo.png" data-src="img/logo.png" alt="logo" width="230" height="36" data-uk-img>
                                    </a>
                                    <!-- module logo begin -->
                                    <p class="uk-text-lead uk-margin-top uk-margin-remove-bottom">Create an Inertia Account</p>
                                    <p class="uk-text-small uk-margin-remove-top uk-margin-medium-bottom">Already have an account? <a href="{{url('login')}}">login here</a></p>
                                    <!-- login form begin -->
                                    <form class="uk-grid uk-form" action="{{ route('register') }}" method="POST" autocomplete="off" id="register_user">
                                          @csrf
                                        <div class="uk-margin-small uk-width-1-1 uk-inline">
                                            <span class="uk-form-icon uk-form-icon-flip fas fa-user fa-sm"></span>
                                            <input class="uk-input uk-border-rounded" id="name" type="text" placeholder="Name" name="name">
                                        </div>
                                         <div class="uk-margin-small uk-width-1-1 uk-inline">
                                            <span class="uk-form-icon uk-form-icon-flip fas fa-user fa-sm"></span>
                                            <input class="uk-input uk-border-rounded" id="surname"   type="text" placeholder="Surname" name="surname">
                                        </div>
                                        <div class="uk-margin-small uk-width-1-1 uk-inline">
                                            <span class="uk-form-icon uk-form-icon-flip fas fa-user fa-sm"></span>
                                            <input class="uk-input uk-border-rounded" id="username"   type="text" placeholder="Choose Username" name="username">
                                        </div>
                                           <div class="uk-margin-small uk-width-1-1 uk-inline">
                                            <span class="uk-form-icon uk-form-icon-flip fas fa-envelope fa-sm"></span>
                                            <input class="uk-input uk-border-rounded" id="email"   type="email" placeholder="Enter Your Email" name="email">
                                        </div>
                                         <div class="uk-margin-small uk-width-1-1 uk-inline">
                                            <span class="uk-form-icon uk-form-icon-flip fas fa-phone fa-sm"></span>
                                            <input class="uk-input uk-border-rounded" id="phone"   type="text" placeholder="Phone Number" name="phone_number">
                                        </div>
                                        <div class="uk-margin-small uk-width-1-1 uk-inline">
                                            <span class="uk-form-icon uk-form-icon-flip fas fa-lock fa-sm"></span>
                                            <input class="uk-input uk-border-rounded" id="password"  type="password" placeholder="Password" name="password">
                                        </div>
                                         <div class="uk-margin-small uk-width-1-1 uk-inline">
                                            <span class="uk-form-icon uk-form-icon-flip fas fa-lock fa-sm"></span>
                                            <input class="uk-input uk-border-rounded" id="password"  type="password" placeholder="Type Password Again" name="password_confirmation">
                                        </div>
                                           <input type="hidden" class="form-control" name="area_id" id="area" value="2">
                                            <input type="hidden" class="form-control"name="referred_by" id="code" value="1">
                                      
                                        
                                        <div class="uk-margin-small uk-width-1-1">
                                            <button class="uk-button uk-width-1-1 uk-button-primary uk-border-rounded uk-float-left" type="submit" name="submit">Create Account</button>
                                        </div>
                                    </form>
                                    <!-- login form end -->
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
    </main>
       

@endsection
