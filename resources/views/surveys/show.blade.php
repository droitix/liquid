@extends('layouts.regapp')

@section('content')
    <h4>Survey: {{$survey->name}}. Stage: {{$stage->stage}}</h4>
    <form action="{{route('surveys.submit_answers')}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="stage_id" value="{{$stage->id}}">
        <input type="hidden" name="survey_id" value="{{$survey->id}}">

        @foreach ($stage->questions as $question)
            @if($question->answer_type == 'text')
                <label for="{{$question->id}}">{{$question->question}}</label>
                <input type="text" name="question_{{$question->id}}" id="question_{{$question->id}}" value="{{rand() . 'is a random number'}}">
            @elseif($question->answer_type == 'number')
                <label for="{{$question->id}}">{{$question->question}}</label>
                <input type="number" name="question_{{$question->id}}" id="question_{{$question->id}}" value="{{rand()}}">
            @elseif($question->answer_type == 'file')
                <label for="{{$question->id}}">{{$question->question}}</label>
                <input type="file" name="question_{{$question->id}}" id="question_{{$question->id}}" value="{{rand() . 'is a random number'}}">
            @endif
            
        @endforeach

        <button type="submit">Submit</button>
    
    </form>

@endsection