    @if (session()->has('impersonate'))
                <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a></li>
                                            <li class="breadcrumb-item active"></li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">ADMIN BIDS VIEW</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
     @foreach ($listings as $listing)
       @if($listing->type())


 @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)



                                  @endforeach

             @if ($listing->amount > $sum)
                     <div class="col-md-6 col-xl-3">
                         <a href="{{ route('listings.apply', [$area, $listing]) }}">

                                <div class="card-box tilebox-one">
                                    <i class="icon-rocket float-right m-0 h2 text-muted"></i>
                                    <h6 class="text-muted text-uppercase mt-0">{{$listing->user->bank}}</h6>
                                    <h3 class="my-3" >K {{$listing->amount - $sum}}</h3>
                                    <h6 class="text-muted text-uppercase mt-0">{{$listing->created_at}}</h6>
                                    <span class="badge badge-warning mr-1"> </span> <span class="text-muted">{{$listing->user->email}}</span>
                                </div>
                                 </a>
                            </div>




            @endif


            @else

        

            @endif
 @endforeach

 @endif