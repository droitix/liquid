@extends('layouts.regapp')
@section('title')
 Applicants for {{$listing->jobtitle}}  | Openjobs360
@endsection
@section('content')
<!-- Our Dashbord -->
    <section class="our-dashbord dashbord">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 dn-smd">
                    <div class="user_profile">
                        <div class="media">
                            <img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="align-self-start mr-3 rounded-circle" alt="e1.png">

                            <div class="media-body">
                                <h5 class="mt-0">Hi, Company Admin</h5>
                                <p>{{ Auth::user()->province }}</p>
                            </div>

                        </div>
                    </div>
                    <div class="dashbord_nav_list">
                          <ul>

                            <li><a href="{{ route('profile') }}"><span class="flaticon-profile"></span> Company Profile</a></li>
                            <li><a class="active" href="{{ route('listings.create', [$area]) }}"><span class="flaticon-resume"></span> Post a New Job</a></li>
                            <li ><a href="{{ route('listings.published.index', [$area]) }}"><span class="flaticon-paper-plane"></span> Manage Live Jobs</a></li>
                            <li><a href="{{ route('listings.favourites.index', [$area]) }}"><span class="flaticon-analysis"></span> Shortlisted Resumes</a></li>
                            <li><a  href="{{ route('listings.viewed.index', [$area]) }}"><span class="flaticon-eye"></span> Jobs You Viewed</a></li>


                            <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="flaticon-logout"></span>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>


                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    <div class="row">
                        <div class="col-lg-12">

                            <h4 class="fz20 mb30">Applicants for your Ad: {{$listing->jobtitle}}</h4>

                        </div>
                        <div class="col-lg-12">
                            <div class="candidate_job_reivew style2">
                                <div class="table-responsive job_review_table mt0">
                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Applicant ID</th>
                                                <th scope="col">Full Name</th>
                                                <th scope="col">Application Date</th>
                                            <th scope="col">Applicant Contact</th>

                                                <th scope="col">View Applicant Profile</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             @foreach ($listing->comments as $comment)
                                            <tr>
                                                <th scope="row">20{{$comment->user_id}}</th>
                                                <td><a href="{{route('profile.index',['user_id'=>$comment->user_id])}}"><u>{{$comment->user->fullname}}</u></a></td>
                                                <td>{{ $comment->created_at->diffForHumans() }}</td>
                                                <td>{{$comment->user->phone}}</td>

                                                <td><li class="list-inline-item"><a href="{{route('profile.index',['user_id'=>$comment->user_id])}}" data-toggle="tooltip" data-placement="bottom" title="View {{$comment->user->fullname}}'s profile"><span class="flaticon-eye"></span></a></li></td>
                                            </tr>
                                             @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection
