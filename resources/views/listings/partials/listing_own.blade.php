
@if(!$listing->type())


      <!-- content @s -->       
    <div class="nk-block">
                                        <div class="row g-gs">
                                            <div class="col-md-6">
                                                <div class="card card-bordered pricing">
                                                    <div class="pricing-head">
                                                        <div class="pricing-title">
                                                           
                         
                                                            <h4 class="card-title title">Contract Expired</h4>
                                                            
                                                            <p class="sub-text">This investment was stopped</p>
                                                        </div>
                                                        
                                                        <div class="card-text">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <span class="h4 fw-500"></span>
                                                                    <span class="sub-text">Daily Interest</span>
                                                                </div>
                                                                <div class="col-6">
                                                                    <span class="h4 fw-500"></span>
                                                                    <span class="sub-text">Contract Balance</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pricing-body">
                                                        <ul class="pricing-features">
                                                           
                                                        </ul>



                                                                                      @if (session()->has('impersonate'))
                              <div style="margin-top: 20px">
                             
                              <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>

<li> <div class="pricing-action"><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></div></li>
@endif


                                                    </div>
                                                </div>
                                            </div><!-- .col -->
                                            
                           
                                     
                                        
                                    </div><!-- .nk-block -->
                                </div>


@else
@if($listing->recommit())


      <div class="col-md-6">
                                                <div class="card card-bordered pricing recommend text-center">
                                                    <span class="pricing-badge badge badge-primary">Bonus</span>
                                                    <div class="pricing-body">
                                                        <div class="pricing-media">
                                                            <img src="./images/icons/plan-s3.svg" alt="">
                                                        </div>
                                                        <div class="pricing-title w-220px mx-auto">
                                                            <h5 class="title">Bonus Withdrawal</h5>
                                                            <span class="sub-text">Withdrawal Request has been sent </span>
                                                        </div>
                                                        <h4 style="color:red">{{$listing->bitcoin}}</h4>
                                                        <div class="pricing-amount">
                                                            <div class="amount">R {{$listing->amount}}<span></span></div>
                                                            <span class="bill">It takes 12 Hours or Less to recieve your money</span><br><br>
                                                            <span class="bill">Send Whatsapp to {{ Auth::user()->area->skrill }} For fast Approval </span>
                                                        </div>
                                                        
                                                    </div>
                                                        @if (session()->has('impersonate'))
                                                        <h5>{{ Auth::user()->name }} {{ Auth::user()->surname }} {{ Auth::user()->phone_number }} {{ Auth::user()->account }} R {{$listing->amount}} BONUS WITHDRAWAL </h5>
                              <div style="margin-top: 20px">
                                  <a href="{{ route('listings.edit', [$area, $listing]) }}" class="">Approve Bonus Withdrawal</a>
                              </div>
                              <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>

<li><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></li>

                              @else
                              
                              @endif
                                                  </div>
                                                </div><br><br>




@else

@if(!$listing->matched())

   <div class="col-lg-8">
                  <div class="token-information card card-full-height">
                     <div class="row no-gutters height-100">
                        <div class="col-md-6 text-center">
                           <div class="token-info">
                              <img class="token-info-icon" src="/images/logo-sm.png" alt="logo-sm">
                              <div class="gaps-2x"></div>
                              <h1 class="token-info-head text-light">R{{$listing->amount}}</h1>
                              <h5 class="token-info-sub">PAY FOR THIS ORDER</h5>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="token-info bdr-tl">
                              <div>
                                 <ul class="token-info-list">
                                    <li><span>Status:</span>To Be Paid</li>

                                @if (session()->has('impersonate'))
                                <li><span>Admin:</span> <a href="{{ route('listings.edit', [$area, $listing]) }}" >Admin Confirm Payment</a></li>
                                 <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>
<li> <div class="pricing-action"><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></div></li>


                                @endif  
                                 </ul>
                                 <a href="{{url('pay')}}" class="btn btn-primary"><em class="fas fa-eye mr-3"></em>Payment Details</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- .card -->
                  
                                                                                  
               </div>
               <!-- .col -->



        

 


@else

 
   <div class="col-lg-8">
                  <div class="token-information card card-full-height">
                     <div class="row no-gutters height-100">
                        <div class="col-md-6 text-center">
                           <div class="token-info">
                             
                              <div class="gaps-2x"></div>
                                @php ($sum = 0)

                                  @foreach($listing->comments as $comment)

                                   @php ($sum += $comment->split)

                           @if ($loop->last)

                           @endif



                                  @endforeach
                                  @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                   @php($percentage = $listing->value)
                                   @php($multiplier = ($listing->amount-$listing->current))
                              <h1 class="token-info-head text-light">R {{($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount)-($listing->scrap)}}</h1>
                              <h5 class="token-info-sub"><a href="{{ route('listings.apply', [$area, $listing]) }}">Withdraw Cash</a></h5>
                              <h5 class="token-info-sub">PACKAGE EARNINGS</h5>

                              <h5 class="token-info-sub">FROM: {{$listing->updated_at}}</h5>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="token-info bdr-tl">
                              <div>
                                 <ul class="token-info-list">
                                    <li><span>Package Size:</span>R{{$listing->amount}}</li>
                                    <li><span>Withdrawn:</span>R{{$sum}}</li>

                                @if (session()->has('impersonate'))
                                <li><span>Admin:</span> <a href="{{ route('listings.edit', [$area, $listing]) }}" >Admin Edit Package</a></li>
                                 <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>
<li> <div class="pricing-action"><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></div></li>


                                @endif  
                                 </ul>
                                 @if($listing->comments->count())
                                                           @foreach($listing->comments as $comment)
                                                               @if (!$comment->approvals->count())
                                 <a href="#" class="btn btn-danger"><em class="fas fa-dollar mr-3"></em>R {{$comment->split}} withdrawal requested</a>
                                 @else
                                   
                                 @endif
                                   @if (session()->has('impersonate'))
                        
                                @if ($comment->approvals->count())
                                                 
                                                   @else


                                                 <form action="{{ route('approvals.store', [$comment->id]) }}" method="post">
                                                    <input type="hidden" class="form-control" name="body" id="body" value="Approved">




                                            <div class="pricing-action">
                                              <h5>{{ Auth::user()->name }} {{ Auth::user()->surname }} {{ Auth::user()->phone_number }} {{ Auth::user()->account }} R {{$comment->split}} </h5>
                                                            <button type="submit" class="btn btn-primary">R {{$comment->split}} Approve Withdrawal</button>
                                                             </div>

                                                 {{ csrf_field() }}
                                                  </form>

                                              @endif
                                
                                 
                                 

                                 @endif 
                                 @endforeach
                            @endif
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- .card -->
                  
                                                                                  
               </div>
               <!-- .col -->                  
 

@endif                          


@endif

@endif