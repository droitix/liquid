<!DOCTYPE html>
<html lang="zxx" dir="ltr">


<!-- Mirrored from www.indonez.com/html-demo/Profit/homepage3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Sep 2021 05:48:33 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta name="description" content="Double your money with liquid forex">
    <meta name="keywords" content="forex, double money, inertia, broker, investment, money doubling">
    <meta name="author" content="Inertia"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#e9e8f0" />
    <!-- Site Properties -->
    <title>Inertia Forex | Home</title>
    <!-- Critical preload -->
    <link rel="preload" href="js/vendors/uikit.min.js" as="script">
    <link rel="preload" href="css/vendors/uikit.min.css" as="style">
    <link rel="preload" href="css/style.css" as="style">
    <!-- Icon preload -->
    <link rel="preload" href="fonts/fa-brands-400.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin>
    <!-- Font preload -->
    <link rel="preload" href="fonts/lato-v16-latin-700.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/lato-v16-latin-regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/montserrat-v14-latin-600.woff2" as="font" type="font/woff2" crossorigin>
    <!-- Favicon and apple icon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon.png">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="css/vendors/uikit.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <!-- preloader begin -->
    <div class="in-loader">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <!-- preloader end -->
    <header>
        <!-- header content begin -->
        <div class="uk-section uk-padding-small in-profit-ticker">
            <div class="uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div data-uk-slider="autoplay: true; autoplay-interval: 5000">
                            <ul class="uk-grid-large uk-slider-items uk-child-width-1-3@s uk-child-width-1-6@m uk-text-center" data-uk-grid>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> XAUUSD <span class="uk-text-success">1478.81</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-down in-icon-wrap small circle down"></i> GBPUSD <span class="uk-text-danger">1.3191</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-down in-icon-wrap small circle down"></i> EURUSD <span class="uk-text-danger">1.1159</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> USDJPY <span class="uk-text-success">109.59</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> USDCAD <span class="uk-text-success">1.3172</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> USDCHF <span class="uk-text-success">0.9776</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-down in-icon-wrap small circle down"></i> AUDUSD <span class="uk-text-danger">0.67064</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> GBPJPY <span class="uk-text-success">141.91</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-section uk-padding-remove-vertical">
            <!-- module navigation begin -->
            <nav class="uk-navbar-container uk-navbar-transparent" data-uk-sticky="show-on-up: true; top: 80; animation: uk-animation-fade;">
                <div class="uk-container" data-uk-navbar>
                    <div class="uk-navbar-left uk-width-auto">
                        <div class="uk-navbar-item">
                            <!-- module logo begin -->
                            <a class="uk-logo" href="{{url('/')}}">
                                <img class="in-offset-top-10" src="/img/logo.png" data-src="/img/logo.png" alt="logo" width="230" height="36" data-uk-img>
                            </a>
                            <!-- module logo begin -->
                        </div>
                    </div>
                    <div class="uk-navbar-right uk-width-expand uk-flex uk-flex-right">
                        <ul class="uk-navbar-nav uk-visible@m">
                            <li><a href="{{url('/')}}">Home</a>
                            </li>
                            
                        </ul>
                        <div class="uk-navbar-item uk-visible@m in-optional-nav">
                            @guest
                            <div>
                                <a href="{{url('/login')}}" class="uk-button uk-button-text">Login</a>
                                <a href="{{url('/register')}}" class="uk-button uk-button-text">Sign up</a>
                            </div>
                            @else

                              <div>
                                <a href="{{url('/dashboard')}}" class="uk-button uk-button-text">Dashboard</a>
                                <a href="{{url('/pay')}}" class="uk-button uk-button-text">Deposit</a>
                            </div>

                            @endguest
                        </div>
                    </div>
                </div>
            </nav>
            <!-- module navigation end -->
        </div>
        <!-- header content end -->
    </header>
    <main>
        <!-- slideshow content begin -->
        <div class="uk-section uk-padding-remove-vertical">
            <div class="in-slideshow uk-visible-toggle" data-uk-slideshow>
                <ul class="uk-slideshow-items">
                    <li>
                        <div class="uk-container">
                            <div class="uk-grid" data-uk-grid>
                                <div class="uk-width-1-2@m">
                                    <div class="uk-overlay">
                                        <h1>Earn securely with us every 5 days including weekends<span class="in-highlight"></span>.</h1>
                                        <p class="uk-text-lead uk-visible@m">Earn 50% in every 5 days and 100% in 10 Days through our legendary forex system.</p>
                                        <div class="in-slideshow-button">
                                            @guest
                                            <a href="{{url('/register')}}" class="uk-button uk-button-primary uk-border-rounded">Open account</a>
                                            @endguest
                                            <a href="{{url('/pay')}}" class="uk-button uk-button-default uk-border-rounded uk-margin-small-left uk-visible@m">Deposit</a>
                                        </div>
                                        <p class="uk-text-small"><span class="uk-text-primary">*</span>Trading in Forex/ CFDs is highly speculative and carries a high level of risk.</p>
                                    </div>
                                </div>
                                <div class="uk-position-center">
                                    <img class="uk-animation-slide-top-small" src="https://www.indonez.com/html-demo/Profit/img/in-slideshow-image-4.png" data-src="https://www.indonez.com/html-demo/Profit/img/in-slideshow-image-4.png" width="862" height="540" data-uk-img>
                                </div>
                            </div>
                        </div>
                    </li>
                   
                </ul>
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-previous data-uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-next data-uk-slideshow-item="next"></a>
                <div class="uk-container in-slideshow-feature uk-visible@m">
                    <div class="uk-grid uk-flex uk-flex-center">
                        <div class="uk-width-3-4@m">
                            <div class="uk-child-width-1-4" data-uk-grid>
                                <div class="uk-flex uk-flex-middle">
                                    <div class="uk-margin-small-right">
                                        <i class="fas fa-drafting-compass in-icon-wrap small circle uk-box-shadow-small"></i>
                                    </div>
                                    <div>
                                        <p class="uk-text-bold uk-margin-remove">24/7 Support</p>
                                    </div>
                                </div>
                                <div class="uk-flex uk-flex-middle">
                                    <div class="uk-margin-small-right">
                                        <i class="fas fa-book in-icon-wrap small circle uk-box-shadow-small"></i>
                                    </div>
                                    <div>
                                        <p class="uk-text-bold uk-margin-remove">Instant Withdrawals</p>
                                    </div>
                                </div>
                                <div class="uk-flex uk-flex-middle">
                                    <div class="uk-margin-small-right">
                                        <i class="fas fa-bolt in-icon-wrap small circle uk-box-shadow-small"></i>
                                    </div>
                                    <div>
                                        <p class="uk-text-bold uk-margin-remove">Fast Response</p>
                                    </div>
                                </div>
                                <div class="uk-flex uk-flex-middle">
                                    <div class="uk-margin-small-right">
                                        <i class="fas fa-percentage in-icon-wrap small circle uk-box-shadow-small"></i>
                                    </div>
                                    <div>
                                        <p class="uk-text-bold uk-margin-remove">10% Bonus</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slideshow content end -->
        <!-- section content begin -->
        <div class="uk-section uk-section-muted in-padding-large-vertical@s in-profit-9">
            <div class="uk-container">
                <div class="uk-grid-divider" data-uk-grid>
                    <div class="uk-width-expand@m in-margin-top-20@s">
                        <h2>Trading products</h2>
                        <p>We are trading any one of the following </p>
                    </div>
                    <div class="uk-width-2-3@m">
                        <div class="uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-6@m uk-text-center" data-uk-grid>
                            <div>
                                <i class="fas fa-euro-sign in-icon-wrap circle uk-box-shadow-small"></i>
                                <p class="uk-text-bold uk-margin-small-top">Forex</p>
                            </div>
                           
                            <div>
                                <i class="fas fa-chart-area in-icon-wrap circle uk-box-shadow-small"></i>
                                <p class="uk-text-bold uk-margin-small-top">Indexes</p>
                            </div>
                            <div>
                                <i class="fas fa-file-contract in-icon-wrap circle uk-box-shadow-small"></i>
                                <p class="uk-text-bold uk-margin-small-top">Stocks</p>
                            </div>
                            <div>
                                <i class="fas fa-tint in-icon-wrap circle uk-box-shadow-small"></i>
                                <p class="uk-text-bold uk-margin-small-top">Energy</p>
                            </div>
                            <div>
                                <i class="fas fa-cube in-icon-wrap circle uk-box-shadow-small"></i>
                                <p class="uk-text-bold uk-margin-small-top">Commodities</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
        <!-- section content begin -->
        <div class="uk-section uk-padding-large in-padding-large-vertical@s in-profit-10">
            <div class="uk-container uk-background-contain uk-background-bottom-right" data-src="img/in-profit-mockup-2.png" data-uk-img>
                <div class="uk-grid" data-uk-grid>
                    <div class="uk-width-1-1 in-offset-bottom-20">
                        <h2 class="uk-margin-small-bottom">Why choose Inertia Forex?</h2>
                        <p class="uk-text-lead uk-margin-remove-top">We offer the best returns for money invested in the whole industry.</p>
                    </div>
                </div>
                <div class="uk-grid-large" data-uk-grid>
                    <div class="uk-width-1-2@s uk-width-1-3@m">
                        <img src="https://www.indonez.com/html-demo/Profit/img/in-profit-icon-1.svg" data-src="https://www.indonez.com/html-demo/Profit/img/in-profit-icon-1.svg" alt="profit-icon" width="86" height="86" data-uk-img>
                        <h4 class="uk-heading-bullet uk-margin-top">You can double</h4>
                        <p>You can double your current amount in less than a week, how great is that?</p>
                    </div>
                    <div class="uk-width-1-2@s uk-width-1-3@m">
                        <img src="https://www.indonez.com/html-demo/Profit/img/in-profit-icon-2.svg" data-src="https://www.indonez.com/html-demo/Profit/img/in-profit-icon-2.svg" alt="profit-icon" width="86" height="86" data-uk-img>
                        <h4 class="uk-heading-bullet uk-margin-top">We are connected</h4>
                        <p>We are a well connected team and you can reach us through several communication platforms.</p>
                    </div>
                    <div class="uk-width-1-2@s uk-width-1-3@m">
                        <img src="https://www.indonez.com/html-demo/Profit/img/in-profit-icon-3.svg" data-src="https://www.indonez.com/html-demo/Profit/img/in-profit-icon-3.svg" alt="profit-icon" width="86" height="86" data-uk-img>
                        <h4 class="uk-heading-bullet uk-margin-top">We are global</h4>
                        <p>Inertia forex is a global initiative, that invests your money responsibly.</p>
                    </div>
                   
                </div>
            </div>
        </div>
        <!-- section content end -->
   
        
    </main>
    <footer>
        <!-- footer content begin -->
        <div class="uk-section uk-section-primary uk-padding-large uk-padding-remove-horizontal uk-margin-medium-top">
            <div class="uk-container">
                <div class="uk-child-width-1-2@s uk-child-width-1-5@m uk-flex" data-uk-grid>
                   
                    
                </div>
               
                    <div class="uk-width-1-2@m in-copyright-text">
                        <p>© Inertia Forex Inc 2022. All rights reserved.</p>
                    </div>
                    <div class="uk-width-1-2@m uk-text-right@m in-footer-socials">
                        <a href="#"><i class="fab fa-youtube"></i></a>
                        <a href="#"><i class="fab fa-facebook-square"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer content end -->
        <!-- module totop begin -->
        <div class="uk-visible@m">
            <a href="#" class="in-totop fas fa-chevron-up" data-uk-scroll></a>
        </div>
        <!-- module totop begin -->
    </footer>
    <!-- Javascript -->
    <script src="js/vendors/uikit.min.js"></script>
    <script src="js/vendors/indonez.min.js"></script>
    <script src="js/config-theme.js"></script>
</body>


<!-- Mirrored from www.indonez.com/html-demo/Profit/homepage3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Sep 2021 05:49:03 GMT -->
</html>