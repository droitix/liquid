 <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta name="description" content="Double your money with inertia forex">
    <meta name="keywords" content="forex, double money, inertia, broker, investment, money doubling">
    <meta name="author" content="Inertia Forex">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#e9e8f0" />
    <!-- Site Properties -->
    <title>Inertia | Account</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon.png">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="/css/vendors/uikit.min.css">
    <link rel="stylesheet" href="/css/style.css">