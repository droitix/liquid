
      <div class="footer-bar">
         <div class="container">
            <div class="row align-items-center justify-content-center">
               <div class="col-md-8">
                  <ul class="footer-links">
                     <li><a href="{{url('privacy')}}">Whitepaper</a></li>
                   
                     <li><a href="{{url('privacy')}}">Privacy Policy</a></li>
                     <li><a href="{{url('privacy')}}">Terms of Condition</a></li>
                  </ul>
               </div>
               <!-- .col -->
               <div class="col-md-4 mt-2 mt-sm-0">
                  <div class="d-flex justify-content-between justify-content-md-end align-items-center guttar-25px pdt-0-5x pdb-0-5x">
                     <div class="copyright-text">&copy; 2021 Global Aid Network.</div>
                     <div class="lang-switch relative">
                        <a href="#" class="lang-switch-btn toggle-tigger">En <em class="ti ti-angle-up"></em></a>
                        
                     </div>
                  </div>
               </div>
               <!-- .col -->
            </div>
            <!-- .row -->
         </div>
         <!-- .container -->
      </div>
      <!-- .footer-bar -->