@extends('layouts.regapp')
@section('title')
 Book CV Writing Service  | Openjobs360
@endsection
@section('content')

<!-- Our Error Page -->
    <section class="our-error bgc-fa" style="margin-top: 30px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-align-left">
                    <div class="error_page newsletter_widget">
                           <h5 class="text-center">Hie, Thank you for your interest in our CV writing service.</h5>

                        <h4 class="text-center"></h4>
                        <h4>Book Via Email</h4>
                         <hr>
                        <h5><i class="fa fa-envelope" aria-hidden="true"></i> Send an email to: <b>cvwriting@openjobs360.co.za</b> And one of our CV writers will respond to you instantly. <a href="#"> </h5>

                        <hr>
                        <h4>Book Via Whatsapp</h4>
                         <hr>
                        <h5><i class="fa fa-whatsapp" aria-hidden="true"></i> Send Message to: <b>0742005469</b> or<a style="color: green;" href="https://wa.me/27742005469"> SEND MESSAGE</a> One of our CV Writers will respond to you instantly.<a href="#"></h5>



                    </div>

                </div>
            </div>
        </div>
    </section>



@endsection
