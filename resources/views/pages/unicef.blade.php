
 
@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


<div class="page-content">
         <div class="container">
            <div class="row">
      
        <!-- .topbar-wrap -->
      <div class="page-content">
         <div class="container">
            <div class="row">
               <div class="main-content col-lg-8">
                  <div class="content-area card">
                     <div class="card-innr">
                        <div class="card-head">
                           <h4 class="card-title card-title-lg">UNICEF SURVEY</h4><br>
                            <a href="{{url('files')}}" class="btn btn-primary"><em class="fas fa-upload mr-3"></em>Upload Survey Picture</a><br>
                           <p>Take a piece of paper and fill in the answers for each question below, when you are done take a picture of your answers and click on upload survey picture and submit.</p>
                        
                        </div>
                        <div class="content">
                           <h4 class="text-secondary">General</h4>
                           <div class="accordion-simple" id="faqList-1">
                              <div class="accordion-item">
                                 <h6 class="accordion-heading collapsed" data-toggle="collapse" data-target="#collapse-1-1">1. What is your name?  </h6>
                                 <div id="collapse-1-1" class="collapse" data-parent="#faqList-1">
                                    <div class="accordion-content">
                                       
                                    </div>
                                 </div>
                              </div>
                              <!-- .accordion-item -->
                              <div class="accordion-item">
                                 <h6 class="accordion-heading collapsed" data-toggle="collapse" data-target="#collapse-1-2">2. Which part of Zambia are you in?</h6>
                                 <div id="collapse-1-2" class="collapse" data-parent="#faqList-1">
                                    <div class="accordion-content">
                                      
                                    </div>
                                 </div>
                              </div>
                              <!-- .accordion-item -->
                              <div class="accordion-item">
                                 <h6 class="accordion-heading collapsed" data-toggle="collapse" data-target="#collapse-1-3">3. How old are you?</h6>
                                 <div id="collapse-1-3" class="collapse" data-parent="#faqList-1">
                                    <div class="accordion-content">
                                      
                                    </div>
                                 </div>
                              </div>
                              <!-- .accordion-item -->
                              <div class="accordion-item">
                                 <h6 class="accordion-heading collapsed" data-toggle="collapse" data-target="#collapse-1-4">4. What do you do for a living?</h6>
                                 <div id="collapse-1-4" class="collapse" data-parent="#faqList-1">
                                    <div class="accordion-content">
                                      
                                    </div>
                                 </div>
                              </div>
                              <!-- .accordion-item -->
                           </div>
                           <!-- .accordion -->
                           <div class="gaps-2x"></div>
                           <h4 class="text-secondary"> Yes | No Questions(Answer with a Yes or a No)</h4>
                           <div class="accordion-simple" id="faqList-2">
                              <div class="accordion-item">
                                 <h6 class="accordion-heading collapsed" data-toggle="collapse" data-target="#collapse-2-1">5. Do you know UNICEF?  </h6>
                                 <div id="collapse-2-1" class="collapse" data-parent="#faqList-2">
                                    <div class="accordion-content">
                                      
                                    </div>
                                 </div>
                              </div>
                              <!-- .accordion-item -->
                              <div class="accordion-item">
                                 <h6 class="accordion-heading collapsed" data-toggle="collapse" data-target="#collapse-2-2">6. Any UNICEF offices in your area?</h6>
                                 <div id="collapse-2-2" class="collapse" data-parent="#faqList-2">
                                    <div class="accordion-content">
                                      
                                    </div>
                                 </div>
                              </div>
                              <!-- .accordion-item -->
                              <div class="accordion-item">
                                 <h6 class="accordion-heading collapsed" data-toggle="collapse" data-target="#collapse-2-3">7. Do you have a school in your Area?</h6>
                                 <div id="collapse-2-3" class="collapse" data-parent="#faqList-2">
                                    <div class="accordion-content">
                                       
                                    </div>
                                 </div>
                              </div>
                              <!-- .accordion-item -->
                              <div class="accordion-item">
                                 <h6 class="accordion-heading collapsed" data-toggle="collapse" data-target="#collapse-2-4">8. Is your family going to school?</h6>
                                 <div id="collapse-2-4" class="collapse" data-parent="#faqList-2">
                                    <div class="accordion-content">
                                       
                                    </div>
                                 </div>
                              </div>
                              <!-- .accordion-item -->
                           </div>
                           <!-- .accordion -->
                           <div class="gaps-2x"></div>
                           <h4 class="text-secondary">Your Area</h4>
                           <div class="accordion-simple" id="faqList-3">
                              <div class="accordion-item">
                                 <h6 class="accordion-heading collapsed" data-toggle="collapse" data-target="#collapse-3-1">9. Do you have a source of water in your area?</h6>
                                 <div id="collapse-3-1" class="collapse" data-parent="#faqList-3">
                                    <div class="accordion-content">
                                       
                                    </div>
                                 </div>
                              </div>
                              <!-- .accordion-item -->
                              <div class="accordion-item">
                                 <h6 class="accordion-heading collapsed" data-toggle="collapse" data-target="#collapse-3-2">10. How far is your source of water</h6>
                                 <div id="collapse-3-2" class="collapse" data-parent="#faqList-3">
                                    <div class="accordion-content">
                                       
                                    </div>
                                 </div>
                              </div>
                             
                           </div>
                           <!-- .accordion -->
                        </div>
                     </div>
                  </div>
                  <!-- .card -->
               </div>
               <!-- .col -->       
         
            </div>
            <!-- .row -->
         </div>
         <!-- .container -->
      </div>
      <!-- .page-content -->

      @endsection