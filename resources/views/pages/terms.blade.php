@extends('layouts.regapp')

@section('title')
  Terms & Privacy Policy | Openjobs360
@endsection

@section('description')
Openjobs360 Terms of Service And Privacy Policy.
@endsection

@section('content')

<!-- Inner Page Breadcrumb -->
    <section class="inner_page_breadcrumb bgc-f0 pt30 pb30" aria-label="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="breadcrumb_title float-left">Terms & Policies</h4>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Terms & Policies</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Our Terms and Policies -->
    <section class="our-terms-policy">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="fz20 mt0">Accuracy of personal information</h4>
                    <p>It is your responsibility to ensure that the information on your profile, CV, supporting documentation and cover letters is accurate and a truthful representation of your employment and work history. You may not create profiles or share personal information on behalf of another person. Openjobs360 will not be held responsible for candidate profiles that are inaccurate or make false claims. Where required by law, Openjobs360 will share fraudulent profiles with the relevant government and legal organisations.</p>
                </div>
                <div class="col-lg-12">
                    <h4 class="fz20">What Personal Information do we collect</h4>
                    <p>The Personal Information we collect about you may include the following:

General Personal Information that can directly or indirectly identify you such as your name, address, contact details, gender, marital status, date and place of birth, and photos.
Sensitive Personal Information (if provided) such as religious beliefs, political affiliations, union membership, sexuality and health information, race (for EE reporting purposes)and criminal records.
Personal Information required in order to provide the service of applying for jobs such as age, location, driver’s license, educational history and employment history.
Your marketing preferences, including opt in for job alert emails, suggested jobs, competition entries, customer service surveys and third party promotions.
Aggregate statistical information relating to site visits, as well as other lawful uses such as job and salary trends. We provide the non-identifiable aggregate statistics to our customers, advertisers and other third parties.
Publicly available information that relates to your educational and employment history which includes, but is not limited to, skills and experience, professional body affiliations, memberships and affiliations as well as qualifications.    </p>
                </div>
                <div class="col-lg-12">
                    <h4 class="fz20">We use Personal Information to do some or all of the following:</h4>
                    <p>Communicate with you as part of our business;
Update you on important information such as changes to our policies, terms and conditions, updates to the Openjobs360 Website or App and other administrative information;
Carry out market research and analysis, including, but not limited to industry trends and satisfaction surveys;
Provide marketing information to you, which may include information about products and services from selected third-party partners. This is done in accordance with your marketing preferences and you may change these at any time in your Openjobs360 account or unsubscribing from these mails when received;
Personalise your Openjobs360 experience on the our Website or App by presenting job opportunities, course suggestions,  advertisements and other group products and services that are tailored to you;
Matching your CV or Openjobs360 profile against any job that you apply for in order to evaluate suitability for the role and highlight the most relevant applications. All applications are delivered and presented to recruiters irrespective of the matching score;
Manage our business operations, and infrastructure, compliance with our internal and group policies and procedures, including, but not restricted to auditing; finance and accounting; collections and billing; IT systems; hosting of data and websites; and records, document and print management; 
Resolution of complaints, and compliance with data access or data correction requests;
Compliance with applicable South African and International laws; compliance with legal processes; responses to requests for information from government and public authorities;
Establishment and defence of legal rights protecting the operations, our rights, privacy, safety or property of Openjobs360 or other group companies or business partners, you or others; and pursue available remedies or limit our damages.</p>
                </div>

                <div class="col-lg-12">
                    <h4 class="fz20">Privacy and Third Parties</h4>
                    <p>This privacy policy does not address, and we are not responsible for, the privacy policy, information use, or other practices of any third party sites to which the Openjobs360 Website or App links. Any link to a third party site does not imply endorsement of the linked site or service by us, Openjobs360 or our group companies. Although we have agreements in place with customers who use Openjobs360 to recruit staff which stipulate that they may only use the service and Personal Information provided to them for the purposes of recruitment, we cannot control their use of the Personal Information provided to them during the course of receiving our services. Our contracts with recruiters and companies stipulate that they not use Personal Information for any reason other than to find a job, but we cannot restrict the customers who have access to the information in our database and Openjobs360 cannot be held responsible for misuse. Openjobs360 has no control of, and cannot be held responsible for any Personal Information that is shared on a third party website or app, even if the job was found on the Openjobs360 Website or App. Please take precautions when sending your CV directly to email addresses, particularly if the email domain does not belong to the hiring company, or if the email belongs to a public email provider such as Gmail, Yahoo or Hotmail. Please ensure the legitimacy of a company before providing any Personal Information.</p>
                </div>
            </div>
        </div>
    </section>





<a class="scrollToHome" href="#"><i class="flaticon-rocket-launch"></i></a>
</div>
<!-- Wrapper End -->


@endsection
