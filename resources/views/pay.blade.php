<!DOCTYPE html>
<html lang="zxx" dir="ltr">


<!-- Mirrored from www.indonez.com/html-demo/Profit/homepage3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Sep 2021 05:48:33 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta name="description" content="Double your money with liquid forex">
    <meta name="keywords" content="forex, double money, inertia, broker, investment, money doubling">
    <meta name="author" content="Inertia"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#e9e8f0" />
    <!-- Site Properties -->
    <title>Inertia Forex | Referals</title>
    <!-- Critical preload -->
    <link rel="preload" href="/js/vendors/uikit.min.js" as="script">
    <link rel="preload" href="/css/vendors/uikit.min.css" as="style">
    <link rel="preload" href="/css/style.css" as="style">
    <!-- Icon preload -->
    <link rel="preload" href="fonts/fa-brands-400.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin>
    <!-- Font preload -->
    <link rel="preload" href="fonts/lato-v16-latin-700.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/lato-v16-latin-regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/montserrat-v14-latin-600.woff2" as="font" type="font/woff2" crossorigin>
    <!-- Favicon and apple icon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon.png">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="/css/vendors/uikit.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>
    <!-- preloader begin -->
    <div class="in-loader">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <!-- preloader end -->
    <header>
        <!-- header content begin -->
        <div class="uk-section uk-padding-small in-profit-ticker">
            <div class="uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div data-uk-slider="autoplay: true; autoplay-interval: 5000">
                            <ul class="uk-grid-large uk-slider-items uk-child-width-1-3@s uk-child-width-1-6@m uk-text-center" data-uk-grid>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> XAUUSD <span class="uk-text-success">1478.81</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-down in-icon-wrap small circle down"></i> GBPUSD <span class="uk-text-danger">1.3191</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-down in-icon-wrap small circle down"></i> EURUSD <span class="uk-text-danger">1.1159</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> USDJPY <span class="uk-text-success">109.59</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> USDCAD <span class="uk-text-success">1.3172</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> USDCHF <span class="uk-text-success">0.9776</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-down in-icon-wrap small circle down"></i> AUDUSD <span class="uk-text-danger">0.67064</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> GBPJPY <span class="uk-text-success">141.91</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-section uk-padding-remove-vertical">
            <!-- module navigation begin -->
            <nav class="uk-navbar-container uk-navbar-transparent" data-uk-sticky="show-on-up: true; top: 80; animation: uk-animation-fade;">
                <div class="uk-container" data-uk-navbar>
                    <div class="uk-navbar-left uk-width-auto">
                        <div class="uk-navbar-item">
                            <!-- module logo begin -->
                            <a class="uk-logo" href="{{url('/dashboard')}}">
                                <img class="in-offset-top-10" src="/img/logo.png" data-src="/img/logo.png" alt="logo" width="230" height="36" data-uk-img>
                            </a>
                            <!-- module logo begin -->
                        </div>
                    </div>
                    <div class="uk-navbar-right uk-width-expand uk-flex uk-flex-right">
                        <ul class="uk-navbar-nav uk-visible@m">
                            <li><a href="{{url('/')}}">Home</a>
                            </li>
                                       @guest

                            @else
                               <li><a href="{{ route('logout') }}" class="nav-link"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"><em class="icon ni ni-signout"></em><span>Logout</span></a>
                                                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                                     </li>
                          @endguest 
                        </ul>
                        <div class="uk-navbar-item uk-visible@m in-optional-nav">
                            @guest
                            <div>
                                <a href="{{url('/login')}}" class="uk-button uk-button-text">Login</a>
                                <a href="{{url('/register')}}" class="uk-button uk-button-text">Sign up</a>
                            </div>
                            @else

                              <div>
                                <a href="{{url('/dashboard')}}" class="uk-button uk-button-text">Dashboard</a>
                                <a href="{{url('/register')}}" class="uk-button uk-button-text">Deposit</a>
                            </div>

                            @endguest
                        </div>
                    </div>
                </div>
            </nav>
            <!-- module navigation end -->
        </div>
        <!-- header content end -->
    </header>
    <main>
        <!-- section content begin -->
        <div class="uk-section">
            <div class="uk-container">
                        
            
       <div class="page-content">
         <div class="container">
            <div class="row">
              
                         
   <div class="col-lg-8">
                  <div class="token-information card card-full-height">
                     <div class="row no-gutters height-100">
                        <div class="col-md-6 text-center">
                           <div class="token-info">
                     
                              <div class="gaps-2x"></div>
                              <h1 class="token-info-head text-light">BANK TRANSFER</h1>
                            <h3>CAPITEC BANK</h3>
                            <h3>ACCOUNT NUMBER: 1913306788</h3>
                            <h3>BRANCH CODE: 470010</h3>

                              <br>
                             
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="token-info bdr-tl">
                              <div>
                                  <h4 style="color: red;" class="token-info-head text-light">INBOX  0628869197 with proof of payment and you will be approved instantly</h4>
                            
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- .card --><hr>
                  <div class="token-information card card-full-height">
                     <div class="row no-gutters height-100">
                        <div class="col-md-6 text-center">
                           <div class="token-info">
                     
                              <div class="gaps-2x"></div>
                              <h1 class="token-info-head text-light">BANK TRANSFER</h1>
                            <h3>FNB BANK</h3>
                            <h3>ACCOUNT NUMBER: 62782760899</h3>
                           
                              <br>
                             
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="token-info bdr-tl">
                              <div>
                                  <h4 style="color: red;" class="token-info-head text-light">INBOX  0628869197 with proof of payment and you will be approved instantly</h4>
                            
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- .card --><hr>
                   <div class="token-information card card-full-height">
                     <div class="row no-gutters height-100">
                        <div class="col-md-6 text-center">
                           <div class="token-info">
                     
                              <div class="gaps-2x"></div>
                              <h1 class="token-info-head text-light">BITCOIN</h1>
                              <h3 class="token-info-head text-light">BITCOIN ADDRESS: bc1qrxgwxyaqcdznntwnr5fn4htz35vnfkgt3p08x9</h3>
                              <br>
                             
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="token-info bdr-tl">
                              <div>
                                  <h4 style="color: red;" class="token-info-head text-light">INBOX  0628869197 with proof of payment and you will be approved instantly</h4>
                            
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- .card -->
               </div>
               <!-- .col -->

     

            
         
            </div>
            <!-- .row -->
         </div>
         <!-- .container -->
      </div>
      <!-- .page-content -->

            </div>
        </div>
        <!-- footer content end -->
        <!-- module totop begin -->
        <div class="uk-visible@m">
            <a href="#" class="in-totop fas fa-chevron-up" data-uk-scroll></a>
        </div>
        <!-- module totop begin -->
    </footer>
    <!-- Javascript -->
    <script src="/js/vendors/uikit.min.js"></script>
    <script src="/js/vendors/indonez.min.js"></script>
    <script src="/js/config-theme.js"></script>
</body>


<!-- Mirrored from www.indonez.com/html-demo/Profit/homepage3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Sep 2021 05:49:03 GMT -->
</html>