<!DOCTYPE html>
<html lang="zxx" dir="ltr">


<!-- Mirrored from www.indonez.com/html-demo/Profit/homepage3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Sep 2021 05:48:33 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta name="description" content="Double your money with liquid forex">
    <meta name="keywords" content="forex, double money, inertia, broker, investment, money doubling">
    <meta name="author" content="Inertia"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#e9e8f0" />
    <!-- Site Properties -->
    <title>Inertia Forex | Home</title>
    <!-- Critical preload -->
    <link rel="preload" href="js/vendors/uikit.min.js" as="script">
    <link rel="preload" href="css/vendors/uikit.min.css" as="style">
    <link rel="preload" href="css/style.css" as="style">
    <!-- Icon preload -->
    <link rel="preload" href="fonts/fa-brands-400.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin>
    <!-- Font preload -->
    <link rel="preload" href="fonts/lato-v16-latin-700.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/lato-v16-latin-regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="fonts/montserrat-v14-latin-600.woff2" as="font" type="font/woff2" crossorigin>
    <!-- Favicon and apple icon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon.png">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="css/vendors/uikit.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <!-- preloader begin -->
    <div class="in-loader">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <!-- preloader end -->
    <header>
        <!-- header content begin -->
        <div class="uk-section uk-padding-small in-profit-ticker">
            <div class="uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div data-uk-slider="autoplay: true; autoplay-interval: 5000">
                            <ul class="uk-grid-large uk-slider-items uk-child-width-1-3@s uk-child-width-1-6@m uk-text-center" data-uk-grid>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> XAUUSD <span class="uk-text-success">1478.81</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-down in-icon-wrap small circle down"></i> GBPUSD <span class="uk-text-danger">1.3191</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-down in-icon-wrap small circle down"></i> EURUSD <span class="uk-text-danger">1.1159</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> USDJPY <span class="uk-text-success">109.59</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> USDCAD <span class="uk-text-success">1.3172</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> USDCHF <span class="uk-text-success">0.9776</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-down in-icon-wrap small circle down"></i> AUDUSD <span class="uk-text-danger">0.67064</span>
                                </li>
                                <li>
                                    <i class="fas fa-angle-up in-icon-wrap small circle up"></i> GBPJPY <span class="uk-text-success">141.91</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-section uk-padding-remove-vertical">
            <!-- module navigation begin -->
            <nav class="uk-navbar-container uk-navbar-transparent" data-uk-sticky="show-on-up: true; top: 80; animation: uk-animation-fade;">
                <div class="uk-container" data-uk-navbar>
                    <div class="uk-navbar-left uk-width-auto">
                        <div class="uk-navbar-item">
                            <!-- module logo begin -->
                            <a class="uk-logo" href="{{url('/dashboard')}}">
                                <img class="in-offset-top-10" src="/img/logo.png" data-src="/img/logo.png" alt="logo" width="230" height="36" data-uk-img>
                            </a>
                            <!-- module logo begin -->
                        </div>
                    </div>
                      
                    <div class="uk-navbar-right uk-width-expand uk-flex uk-flex-right">
                        <ul class="uk-navbar-nav uk-visible@m">
                            
                            <li><a href="{{url('/')}}">Home</a>
                            </li>
                            <li><a href="{{url('/pay')}}">Deposit</a>
                            </li>
                            
                                  @role('admin')
                              <li><a href="{{ url('admin/impersonate')}}">impersonate</a>
                            </li>
                            <li><a href="{{ url('admin/users')}}">Users</a>
                            </li>
                            <li><a href="{{ url('admin/listings')}}">Listings</a>
                            </li>
                           
                            </li>
                              @if (session()->has('impersonate'))
                                <li><a href="{{ route('listings.create', [$area]) }}">Create</a>
                            </li>
                               <li>
                                <a href="#" onclick="event.preventDefault(); document.getElementById('impersonating').submit();">
                                    <i class="mdi mdi-lock"></i>
                                    <span> Stop Impersonating </span>
                                </a>
                                <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                           </li>
                            @endif


                            @endrole
                             <li><a href="{{url('/dashboard')}}">Dashboard</a>
                            </li>
                            <li><a href="{{url('referrals')}}">Downliners</a>
                            </li>
                  
                  
                        </ul>
                        <div class="uk-navbar-item uk-visible@m in-optional-nav">
                            @guest
                            <div>
                                <a href="{{url('/login')}}" class="uk-button uk-button-text">Login</a>
                                <a href="{{url('/register')}}" class="uk-button uk-button-text">Sign up</a>
                            </div>
                            @else

                              <div>
   
                         
                                <a href="{{url('/dashboard')}}" class="uk-button uk-button-text">Dashboard</a>
                                <a href="{{url('/register')}}" class="uk-button uk-button-text">Deposit</a>
                            </div>

                            @endguest
                        </div>
                    </div>
                </div>
            </nav>
            <!-- module navigation end -->
        </div>
        <!-- header content end -->
    </header>
    <main>
        <!-- section content begin -->
        <div class="uk-section">
            <div class="uk-container">
                <div class="uk-grid-match uk-grid-medium uk-child-width-1-2@s uk-child-width-1-3@m in-card-10" data-uk-grid>
                    <div class="uk-width-1-1 uk-flex uk-flex-center">
                        <div class="uk-width-3-5@m uk-text-center">
                            <h1 class="uk-margin-remove">Client Dashboard.</h1>
                            <p class="uk-text-lead uk-text-muted uk-margin-remove">Your Link:  {{$referralink}}</p><hr>


                            <p><a  href="{{ route('logout') }}"
        onclick="event.preventDefault();
         document.getElementById('logout-form').submit();">
        <i class="fe fe-logout"></i>{{ __('Signout') }}
                                        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                     @csrf
                </form>
                         </p>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card uk-card-default uk-card-body uk-border-rounded uk-light in-card-green">
                            <i class="fas fa-seedling fa-lg in-icon-wrap circle uk-margin-bottom"></i>
                            <h4 class="uk-margin-top">
                                <a href="#">Account Profit<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h4>
                      
                                            @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->value)

                                        @php($multiplier = ($listing->amount-$listing->current))
 
                             
                            @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach
                    

                        @php ($balance = $sum-$diff)
                             <h3 class="uk-margin-top">
                                <a href="#">R {{$balance}}<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h3>
                            <hr>
                             <h4 class="uk-margin-top">
                                <a href="#">Bonus Balance<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h4>
                              @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach
                             <h3 class="uk-margin-top">
                                <a href="#">R {{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-($withdrawn)}}<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h3>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card uk-card-default uk-card-body uk-border-rounded uk-light in-card-blue">
                            <i class="fas fa-chart-bar fa-lg in-icon-wrap circle uk-margin-bottom"></i>
                            <h4 class="uk-margin-top">
                                <a href="{{ route('listings.published.index', [$area]) }}">View Your Transactions <i class="fas fa-chevron-right uk-float-right"></i></a>
                               
                            </h4>
                            <hr>
                             <h3 class="uk-margin-top">
                                <a href="#">{{ Auth::user()->referrals()->count()}}<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h3>
                             <h4 class="uk-margin-top">
                                <a href="{{url('referrals')}}">View Your Downliners <i class="fas fa-chevron-right uk-float-right"></i></a>
                               
                            </h4>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card uk-card-default uk-card-body uk-border-rounded uk-light in-card-grey">
                            <i class="fas fa-chart-pie fa-lg in-icon-wrap circle uk-margin-bottom"></i>
                            <h4 class="uk-margin-top">
                                <a href="{{url('/pay')}}">DEPOSIT <i class="fas fa-chevron-right uk-float-right"></i></a>

                               
                            </h4>
                            <hr>
                           
                          <h4 class="uk-margin-top">
                                <a href="{{ route('listings.published.index', [$area]) }}">WITHDRAW MONEY <i class="fas fa-chevron-right uk-float-right"></i></a>

                               
                            </h4><hr>
                            <h4 class="uk-margin-top">
                                <a href="{{ url('profile') }}">EDIT PROFILE <i class="fas fa-chevron-right uk-float-right"></i></a>

                               
                            </h4><hr>
                            @if (session()->has('impersonate'))
                            <h4 class="uk-margin-top">
                                <a href="{{ route('listings.create', [$area]) }}">CREATE <i class="fas fa-chevron-right uk-float-right"></i></a>

                               
                            </h4>
                            @endif
                        </div>
                    </div>

            
                </div>
            </div>
        </div>
        <!-- section content end -->
       
    </main>
    <footer>
        <!-- footer content begin -->
        <div class="uk-section uk-section-primary uk-padding-large uk-padding-remove-horizontal uk-margin-medium-top">
            <div class="uk-container">
                <div class="uk-child-width-1-2@s uk-child-width-1-5@m uk-flex" data-uk-grid>
                   
                    
                </div>
               
                    <div class="uk-width-1-2@m in-copyright-text">
                        <p>© Inertia Forex Inc 2021. All rights reserved.</p>
                    </div>
                    <div class="uk-width-1-2@m uk-text-right@m in-footer-socials">
                        <a href="#"><i class="fab fa-youtube"></i></a>
                        <a href="#"><i class="fab fa-facebook-square"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer content end -->
        <!-- module totop begin -->
        <div class="uk-visible@m">
            <a href="#" class="in-totop fas fa-chevron-up" data-uk-scroll></a>
        </div>
        <!-- module totop begin -->
    </footer>
    <!-- Javascript -->
    <script src="js/vendors/uikit.min.js"></script>
    <script src="js/vendors/indonez.min.js"></script>
    <script src="js/config-theme.js"></script>
</body>


<!-- Mirrored from www.indonez.com/html-demo/Profit/homepage3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Sep 2021 05:49:03 GMT -->
</html>