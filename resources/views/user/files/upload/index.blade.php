@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 <!-- content @s -->
                <div class="nk-content nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-body">
                            <div class="buysell wide-xs m-auto">
                                <div class="buysell-nav text-center">
                                    <ul class="nk-nav nav nav-tabs nav-tabs-s2">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('listings.create', [$area]) }}">Survey Submission</a>
                                        </li>
                                       
                                    </ul>
                                </div><!-- .buysell-nav -->
                                <div class="buysell-title text-center">
                                    <h4 class="title">Submit Survey</h4>
                                </div><!-- .buysell-title -->
                                <div class="buysell-block">
                                     <form action="/uploaddocs" method="post" enctype="multipart/form-data">
{{ csrf_field() }}

                                <div class="form-group">

                                            <input type="hidden" name="name" value="0" class="form-control">

                                        </div>


<br />
<input type="file" class="form-control" name="cvs[]" multiple />
<br />
<input type="submit" class="btn  btn-success" value="Upload " />

</form>
                                </div><!-- .buysell-block -->
                            </div><!-- .buysell -->
                        </div>
                    </div>
                </div>
                <!-- content @e -->


@endsection
