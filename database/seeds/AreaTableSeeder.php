<?php

use Illuminate\Database\Seeder;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [
            [
            'name' => 'zambia',
                'children' => [
                    [
                        'name' => 'Johannesburg',
                        'children' => [
                            ['name' => 'Alexandra'],
                            ['name' => 'Bram Fischerville'],
                            ['name' => 'ah'],
                            ['name' => 'sw'],
                            ['name' => 'pk'],
                            ['name' => 'sf'],

                        ],
                    ],


                    [
                        'name' => 'North West',
                        'children' => [
                            ['name' => 'Bloemhof'],

                         ],
                    ],
                ],
            ],
        ];

        foreach ($areas as $area) {
            \openjobs\Area::create($area);
        }
    }
}
