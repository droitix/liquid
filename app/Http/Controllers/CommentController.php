<?php

namespace openjobs\Http\Controllers;

use Auth;
use Session;
use openjobs\Area;
use openjobs\Comment;
use openjobs\Listing;
use Illuminate\Http\Request;
use openjobs\Twilio\SendSmsNotification;
use openjobs\Http\Requests\StoreCommentFormRequest;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreCommentFormRequest $request, $listing_id)
    {
        $listing = Listing::find($listing_id);
        $sum = 0;
        $now = \Carbon\Carbon::now();
        $days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now);
        
        foreach ($listing->comments as $comment) {
            $sum += $comment->split;

            $sum;
        }
        if (($listing->amount-$sum)+(($listing->amount)*($listing->value)*$days) < $request->split) {
            return back();
        } else {
            $comment = new Comment();
            $comment->body = $request->body;
            $comment->split = $request->split;
            $comment->user_id = auth()->user()->id;
            $comment->category_id = $request->category_id;
            $comment->listing()->associate($listing);
            $comment->save();

           // (new SendSmsNotification)->sendSms(auth()->user()->phone_number, "Thank you, you have requested a withdrawal of {{$comment->split}}.");

            return back()->withSuccess('Your bid was successfull!!');
        }
    }


    public function destroy(Area $area, Comment $comment)
    {
        $this->authorize('destroy', $comment);

        $comment->delete();

        return back()->withSuccess('Comment was deleted.');
    }

    public function show(Request $request, Area $area, Comment $comment)
    {
        return view('comments.show', compact('comment'));
    }

    public function bitshow(Request $request, Area $area, Comment $comment)
    {
        return view('comments.bitshow', compact('comment'));
    }

    public function sell(Request $request, Comment $comment)
    {
        return view('sell', compact('comment'));
    }
}
