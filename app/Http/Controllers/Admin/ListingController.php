<?php

namespace openjobs\Http\Controllers\Admin;


use Illuminate\Http\Request;
use openjobs\Http\Requests\StoreListingFormRequest;
use openjobs\{Area, Category, Listing};
use openjobs\User;
use openjobs\Http\Controllers\Controller;

class ListingController extends Controller
{
    public function update(StoreListingFormRequest $request, Area $area, Listing $listing)
    {
        $this->authorize('update', $listing);

         $listing->maturityamount = $request->maturityamount;

        if (!$listing->live()) {
            $listing->category_id = $request->category_id;

        }

        $listing->area_id = $request->area_id;

        $listing->live = true;
        $listing->created_at = \Carbon\Carbon::now();
        $listing->save();



       return redirect()->back();
    }

  public function display()
    {
        $listings = Listing::with(['user', 'area'])->latestFirst()->paginate(1000);
        return view('admin.listings.index')->with(compact('listings'));
    }

     public function invested()
    {
        $listings = Listing::with(['user', 'area'])->latestFirst()->paginate(1000);
        return view('admin.listings.invested')->with(compact('listings'));
    }

         public function withdrawal()
    {
        $listings = Listing::with(['user', 'area'])->latestFirst()->paginate(1000);
        return view('admin.listings.withdrawals')->with(compact('listings'));
    }

     public function destroyListing($id)
    {
        Listing::where('id', $id)->delete();
        session()->flash('message', 'Listing deleted!');
         notify()->success('listing Deleted');
        return redirect()->back();
    }


}
